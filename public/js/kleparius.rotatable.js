function preventSelection(element){
    var preventSelection = false;

    function addHandler(element, event, handler){
        if (element.attachEvent) 
            element.attachEvent('on' + event, handler);
        else 
            if (element.addEventListener) 
                element.addEventListener(event, handler, false);
    }
    function removeSelection(){
        if (window.getSelection) { window.getSelection().removeAllRanges(); }
        else if (document.selection && document.selection.clear)
            document.selection.clear();
    }
    function killCtrlA(event){
        var event = event || window.event;
        var sender = event.target || event.srcElement;

        if (sender.tagName.match(/INPUT|TEXTAREA/i))
            return;

        var key = event.keyCode || event.which;
        if (event.ctrlKey && key == 'A'.charCodeAt(0))    // 'A'.charCodeAt(0) можно заменить на 65
        {
            removeSelection();

            if (event.preventDefault) 
                event.preventDefault();
            else
                event.returnValue = false;
        }
    }

    // не даем выделять текст мышкой
    addHandler(element, 'mousemove', function(){
        if(preventSelection)
            removeSelection();
    });
    addHandler(element, 'mousedown', function(event){
        var event = event || window.event;
        var sender = event.target || event.srcElement;
        preventSelection = !sender.tagName.match(/INPUT|TEXTAREA/i);
    });

    // борем dblclick
    // если вешать функцию не на событие dblclick, можно избежать
    // временное выделение текста в некоторых браузерах
    addHandler(element, 'mouseup', function(){
        if (preventSelection)
            removeSelection();
        preventSelection = false;
    });

    // борем ctrl+A
    // скорей всего это и не надо, к тому же есть подозрение
    // что в случае все же такой необходимости функцию нужно 
    // вешать один раз и на document, а не на элемент
    addHandler(element, 'keydown', killCtrlA);
    addHandler(element, 'keyup', killCtrlA);
}
$.fn.hasParent = function(objs) {
    // ensure that objs is a jQuery array
    objs = $(objs); var found = false;
    $(this[0]).parents().andSelf().each(function() {
        if ($.inArray(this, objs) != -1) {
            found = true;
            return false; // stops the each...
        }
    });
    return found;
}
// HELL
$.fn.rotatable = function() {
    var r = $(this); // вращаемый блок
    var o = r.offset(); // отступы от краев экрана этот блока
    r.prepend('<div class="rotate_point"></div>'); // добавили пимпу
    var point = $(this).find(".rotate_point"); // пимпа
    var margin_top = -40;
    point.css({ top: margin_top, left: 130 });
    point.hide();
    r.prepend('<div class="rotate_wrapper"></div>'); // добавили враппер, нужен для ховер
    var wrapper = $(this).find(".rotate_wrapper");
    wrapper.css({ 'margin-top': margin_top, 'padding-bottom': -margin_top });
    var clicking = false; // если кликнули по пимпе

    wrapper.hover(function(){
        point.show();
        console.log('show');
    }, function(){
        point.hide();
        console.log("hide");
    });
    // console.log($(".rotate_point"));
    $(".rotate_point").mousedown(function(e) {
        e.preventDefault();
        clicking = true;
        console.log('клик по пимпе');
        var top = $( ".rotate_point").offset.top;
        var left = $( ".rotate_point").offset.left;
        console.log(top + ' ' + left);
        $( ".rotate_point" ).prependTo('kl-page');
        $( ".rotate_point").css({top: top, left: left});
    });
    $(document).mouseup(function(e) {
        if (clicking = true) {
            // point.text(''); // не отображать градусы
        }
        clicking = false;
    });
    $(document).mousemove(function(e) {
        if(clicking == false) return;
        console.log('тянем пимпу');
        e.preventDefault();
        // preventSelection(document);
        // mousedown + mousemove одновременно
        var center_x = (o.left) + (r.width()/2) + 33; // координаты положения точки
        var center_y = (o.top) + (r.height()/2) + 33; // 33 магическое число
        var mouse_x = e.pageX; var mouse_y = e.pageY; // координаты положения мыши
        var radians = Math.atan2(mouse_x - center_x, mouse_y - center_y);
        degree = Math.round( (radians * (180 / Math.PI) * -1) + 180); // немного математики
        r.css('-moz-transform', 'rotate('+degree+'deg)');
        r.css('-webkit-transform', 'rotate('+degree+'deg)');
        r.css('-o-transform', 'rotate('+degree+'deg)');
        r.css('-ms-transform', 'rotate('+degree+'deg)');
        // point.text(degree + '°');
    });
}
$(".rotatable").click(function(){
    console.log("клик по ротатабле");
});