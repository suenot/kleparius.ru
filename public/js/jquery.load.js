$(document).ready(function(){
	var firstLoad = false;
	$('<div id="load_article"></div>').insertBefore('load');
	$('load').appendTo('#load_article');
	$('<div id="load_nav"></div>').insertBefore('nav');
	$('nav').appendTo('#load_nav');
	$('nav li.left').find('a').live('click', function(e){
		e.preventDefault();
		var href = $(this).attr('href');
		$('#load_article').fadeOut().load(href + ' load', function(){
			kl_settings();
		}).fadeIn();
		history.pushState({foo: 'bar'}, 'Title', href);
	});
	$('#lang_panel').find('a').live('click', function(e){
		e.preventDefault();
		var href = $(this).attr('href');
		$('#load_nav').load(href + ' nav');
		$("#lang_panel").hide();
		$('#load_article').fadeOut().load(href + ' load').fadeIn();
		history.pushState({foo: 'bar'}, 'Title', href);
	});
	window.onpopstate = function(e) {
		if (!firstLoad) {
			firstLoad = true;
		}
		else {
			window.location.reload();
		}
	};
});