function kl_settings () {
    $(".resizable").resizable({ containment: "kl-page" }); // создает .ui-wrapper для kl-text
    $("kl-img.draggable").parent(".ui-wrapper").draggable({ containment: "kl-page", cancel: "no-draggable" })
    $("kl-text.draggable").draggable({ containment: "kl-page" });
    $( ".rotatable" ).rotatable();
    // $( "no-draggable" ).prepend('<div class="rotate_point"></div>');
    // $( ".rotate_point").css({top: '-40px', left: parseInt($( '.rotatable' ).css('width'))/2});
}
$(document).ready(function(){
    kl_settings();
});