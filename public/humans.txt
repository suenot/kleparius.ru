/* the humans responsible & colophon */
/* humanstxt.org */


/* TEAM */
  html/css: suenot, thatmean
  Site: http://www.itproduction.net/en/
  Location: http://www.itproduction.net/en/contacts/

/* THANKS */
  Names (& URL): 

/* SITE */
  Standards: html5, css3
  Syntax: sass, erb
  Compiler: serve
  Components: modernizr, boilerplate, jQuery, compass, lemonade, de-core(alpha)
  Software: sublime2
  

                                    
                               -o/-                       
                               +oo//-                     
                              :ooo+//:                    
                             -ooooo///-                   
                             /oooooo//:                   
                            :ooooooo+//-                  
                           -+oooooooo///-                 
           -://////////////+oooooooooo++////////////::    
            :+ooooooooooooooooooooooooooooooooooooo+:::-  
              -/+ooooooooooooooooooooooooooooooo+/::////:-
                -:+oooooooooooooooooooooooooooo/::///////:-
                  --/+ooooooooooooooooooooo+::://////:-   
                     -:+ooooooooooooooooo+:://////:--     
                       /ooooooooooooooooo+//////:-        
                      -ooooooooooooooooooo////-           
                      /ooooooooo+oooooooooo//:            
                     :ooooooo+/::/+oooooooo+//-           
                    -oooooo/::///////+oooooo///-          
                    /ooo+::://////:---:/+oooo//:          
                   -o+/::///////:-      -:/+o+//-         
                   :-:///////:-            -:/://         
                     -////:-                 --//:        
                       --                       -:        
