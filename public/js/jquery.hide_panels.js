$(document).ready(function() {
    $("#login_panel, #lang_panel").hide();
    $("#login").click(function(e) {
        e.preventDefault();
        $("#lang_panel").hide();
        $("#login_panel").toggle();
    });
    $("#lang").live("click", function(e) {
        e.preventDefault();
        $("#login_panel").hide();
        $("#lang_panel").toggle();
    });
});