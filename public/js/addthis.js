var addthis_config = {
    ui_language: "en",
    ui_click: true,
    services_custom: {
        name: "Я.ру",
        url: "http://my.ya.ru/posts_add_link.xml?URL={{url}}&title={{title}}",
        icon: "//mrlater.ru/images/ya.png"
    }
};